# Graphic generator
Projekt do predmetu Výtvarná informatika.
Aplikácia umožňuje uživateľovi generovať grafické výstupy, ktoré patria do generovanej grafiky.


## Návod
Tieto inštrukcie slúžia k jednoduchej inštalácií a spusteniu aplikácie pre vývoj a testovanie.

### Prerekvizity
Potrebné nástroje pre spustenie aplikácie.

```
* [Node LTS](https://nodejs.org/en/)
* Yarn
```

### Inštalácia

Naištalovanie závislostí

```
yarn install
```

Spustenie DEV build

```
yarn build:dev
```
Spustenie DEV server

```
yarn start:dev
```
Lokálny DEV server beží na **porte 3000**.
#### Produkcia

Spustenie PROD build
```
yarn build
```
Spustenie PROD server

```
yarn start
```
Produkčný server beží na **porte 3000**.
## Uživateľský návod


Uživateľ môže generovať grafické výstupy pomocou uživateľského rozhrania (**Settings**).
V nastavení je možné:
 
* Uřčiť množstvo dlaždíc na X a Y osy (definuje sa pomocou číselnej hodnoty)
* Vybrať metódu vykreslovania
* Určiť poradie a typy dlaždíc, ktoré sa majú použiť (definuje sa pomocou čísel oddelených ",")
* Nastaviť pozadie pre objekty (Foreground) a plátno (Background)

Tlačítko **Export** umožňuje uživateľovy exportovať vygenerovaný grafický výstup.

## Použité nástroje

* [React](https://reactjs.org/) - Fronted dizajn

## Autor

* **Norbert Durcansky** - https://bitbucket.org/norbertdurcansk/vin/src/master/
xdurca01 VUTBR FIT 2018 

## Licence

Tento projekt je pod MIT Licenciou



